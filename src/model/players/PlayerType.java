package model.players;

/**
 * 
 * @author Stefano Benelli
 * This enum represents the list of Player Types
 */
public enum PlayerType {
	HUMAN,
	CPU
}
