package model.players;

/**
 * 
 * @author Stefano Benelli
 * Implementation of CPU Encoder
 */
public class EncoderCpu extends EncoderBase {

	private static final long serialVersionUID = 2818249808990229088L;

	public EncoderCpu(String name) {
		super(name, PlayerType.CPU);
	}
}
